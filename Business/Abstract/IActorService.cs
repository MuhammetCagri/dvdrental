﻿using Entities.Concrate;
using System;
using System.Collections.Generic;
using System.Text;
using Core.Utilities.Result;
using Entities.Concrate.Actors.Request;
using Entities.Concrate.Actors.Response;

namespace Business.Abstract
{
    public interface IActorService
    {
        //Buraya Operasyonlarımız eklenir.
        //Repositoriye uygun değildir.
        IDataResult<List<ActorGetResponse>> GetList();
        IDataResult<List<ActorGetResponse>> GetByFirstName(string firstName);
        IDataResult<ActorGetResponse> GetById(int id);
        IResult Add(ActorRequest actor);
        IResult Update(ActorUpdateRequest updateRequest);
        IResult Delete(int id);

    }
}
