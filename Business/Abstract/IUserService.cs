﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.Entities.Concrate;
using Core.Utilities.Result;
using Core.Utilities.Security.Jwt;
using Entities.Concrate;

namespace Business.Abstract
{
    public interface IUserService
    {
        List<OperationClaim> GetClaims(User user);
        void Insert(User user);
        User GetByEmail(string mail);
    }
}
