﻿using System;
using System.Collections.Generic;
using System.Text;
using Business.Abstract;
using Core.Entities.Concrate;
using Core.Utilities.Hashing;
using Core.Utilities.Result;
using Core.Utilities.Security.Jwt;
using Entities.Concrate.Dtos;

namespace Business.Concrete
{
    public class AutManager:IAuthService
    {
        private IUserService _userService;
        private ITokenHelper _tokenHelper;

        public AutManager(IUserService userService,ITokenHelper tokenHelper)
        {
            _userService = userService;
            _tokenHelper = tokenHelper;
        }
        public IDataResult<User> Register(UserForRegisterDto userForRegisterDto, string password)
        {
            byte[] passwordHash, passwordSalt;
            HashingHelper.CratePasswordHash(password,out passwordHash,out passwordSalt);
            var user=new User
            {
                email = userForRegisterDto.Email,
                active = true,
                first_name = userForRegisterDto.FirtName,
                last_name = userForRegisterDto.LastName,
                password_hash = passwordHash,
                password_salt = passwordSalt
            };
            _userService.Insert(user);

            return new SuccessDataResult<User>(user);
        }

    

        public IDataResult<User> Login(UserForLoginDto userForRegisterDto)
        {
           var chk= _userService.GetByEmail(userForRegisterDto.Email);
           if (chk==null)
           {
               return new ErrorDataResult<User>("Not Found User");
           }

           if (!HashingHelper.VerifyPasswordHash(userForRegisterDto.Password,chk.password_hash,chk.password_salt))
           {
               return new ErrorDataResult<User>("Verify is not successful");
           }

           return new SuccessDataResult<User>(chk, "Login is successful");
        }

        public IResult UserExists(string email)
        {
            if (_userService.GetByEmail(email)!=null)
            {
                return new ErrorResult("Already,There is this user");
            }
            return new SuccessResult("Okay");
        }

        public IDataResult<AccessToken> CreateAccessToken(User user)
        {
            var claims = _userService.GetClaims(user);
            var accessToken = _tokenHelper.CreateToken(user,claims);
            return  new SuccessDataResult<AccessToken>(accessToken);
        }
    }
}
