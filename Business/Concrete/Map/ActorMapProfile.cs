﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using Entities.Concrate.Actors;
using Entities.Concrate.Actors.Request;
using Entities.Concrate.Actors.Response;

namespace Business.Concrete.Map
{
    public class ActorMapProfile :Profile
    {
        public ActorMapProfile()
        {
            CreateMap<ActorRequest, Actors>()
                .ForMember(x => x.last_name, y => y.MapFrom(z => z.LastName))
                .ForMember(x => x.first_name, y => y.MapFrom(z => z.FirstName));

            CreateMap<ActorUpdateRequest, Actors>()
                .ForMember(x => x.last_name, y => y.MapFrom(z => z.LastName))
                .ForMember(x => x.first_name, y => y.MapFrom(z => z.FirstName))
                .ForMember(x => x.actor_id, y => y.MapFrom(z => z.Id));
            
            CreateMap<Actors, ActorGetResponse>()
                .ForMember(x => x.LastName, y => y.MapFrom(z => z.last_name))
                .ForMember(x => x.FirstName, y => y.MapFrom(z => z.first_name));

        }
    }
}
