﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Business.Abstract;
using Core.Exceptions;
using Core.Utilities.Result;
using DataAccess.Abstract;
using Entities.Concrate;
using Entities.Concrate.Actors;
using Entities.Concrate.Actors.Request;
using Entities.Concrate.Actors.Response;

namespace Business.Concrete
{
    //Dependecy Injectionu burada kullancağız.
    //IoC conteiner olarak Autofac kullanıcak.
    //Buraya Validation ve iş kurallarımız gerebilir.
    public class ActorManager : IActorService
    {
        private readonly IActorDal _actorDal;
        private readonly IMapper _mapper;
        HandleException handleException = new HandleException();
        public ActorManager(IActorDal actorDal,IMapper mapper)
        {
            _actorDal = actorDal;
            _mapper = mapper;
        }

        public IDataResult<ActorGetResponse> GetById(int id)
        {
            try
            {
                var actors = _actorDal.Get(a => a.actor_id == id);
                var map = _mapper.Map<ActorGetResponse>(actors);
                return new SuccessDataResult<ActorGetResponse>(map);
            }
            catch (Exception e)
            {
                return new ErrorDataResult<ActorGetResponse>(e.Message);
            }
        }

        public IResult Add(ActorRequest request)
        {
            try
            {
                var map = _mapper.Map<Actors>(request);
                _actorDal.Add(map);
                return new ErrorResult(handleException.Message);
            }
            catch (Exception )
            {
                return new SuccessResult("adding to database is successed");
            }

        }

        public IResult Delete(int id)
        {
            try
            {
                _actorDal.Delete(a => a.actor_id == id);
                return new SuccessResult("deleting to database is successed");
            }
            catch (Exception )
            {
                return new ErrorResult(handleException.Message);
            }
        }

        public IDataResult<List<ActorGetResponse>> GetByFirstName(string firstName)
        {
            try
            {
                var actors = _actorDal.GetByName(a => a.first_name.Contains(firstName));
                var map = _mapper.Map<List<ActorGetResponse>>(actors);
                return new SuccessDataResult<List<ActorGetResponse>>(map);

            }
            catch (Exception e)
            {
                return new ErrorDataResult<List<ActorGetResponse>>(handleException.Message);
            }

        }

        public IDataResult<ActorGetResponse> GetByActorId(int actorId)
        {
            try
            {
                var actors = _actorDal.Get(a => a.actor_id == actorId);
                var map = _mapper.Map<ActorGetResponse>(actors);
                return new SuccessDataResult<ActorGetResponse>(map);

            }
            catch (Exception e)
            {
                return new ErrorDataResult<ActorGetResponse>(e.Message);
            }

        }

        public IDataResult<List<ActorGetResponse>> GetList()
        {
            try
            {
                var entities = _actorDal.GetList().ToList();
                var map = _mapper.Map<List<ActorGetResponse>>(entities);
                    if(map==null)
                        return new ErrorDataResult<List<ActorGetResponse>>("Kayıt yok"); ;
                return new SuccessDataResult<List<ActorGetResponse>>(map);
            }
            catch (Exception e)
            {
                return new ErrorDataResult<List<ActorGetResponse>>(handleException.Message);
            }

        }

        public IResult Update(ActorUpdateRequest updateRequest)
        {
            try
            {
                var map = _mapper.Map<Actors>(updateRequest);
                _actorDal.Update(map);
                return new SuccessResult("Updating to Database is Successed");
            }
            catch (Exception e)
            {
                return new ErrorResult(handleException.Message);
            }
        }
    }
}
