﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Business.Abstract;
using Core.Entities.Concrate;
using Core.Exceptions;
using Core.Utilities.Result;
using DataAccess.Abstract;
using Entities.Concrate;

namespace Business.Concrete
{
    public class UserManager : IUserService
    {
        private readonly IUserDal _userDal;
        HandleException _handleException = new HandleException();
        public UserManager(IUserDal userDal)
        {
            _userDal = userDal;
        }

        public User GetByEmail(string mail)
        {

            return _userDal.Get(a => a.email == mail);



        }

        public List<OperationClaim> GetClaims(User user)
        {


            return _userDal.GetClaims(user).ToList();

        }

        public void Insert(User user)
        {

            _userDal.Add(user);

        }
    }
}
