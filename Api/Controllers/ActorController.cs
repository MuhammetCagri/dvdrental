﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Core.Utilities.Result;
using Entities.Concrate;
using Entities.Concrate.Actors.Request;
using Entities.Concrate.Actors.Response;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [ApiController]
    [Route("actors")]
    [Produces("application/json")]
    public class ActorController : ControllerBase
    {
        private IActorService _actorService;

        public ActorController(IActorService actorService)
        {
            _actorService = actorService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IDataResult<List<ActorGetResponse>>), 200)]
        public IActionResult GetList()
        {
             var result = _actorService.GetList();
             if (result.Success)
             {
                 return Ok(result.Data);
             }
             else
             {
                 return BadRequest(result.Message);

             }
        }

        [HttpGet("name/{firstName}")]
        [ProducesResponseType(typeof(IDataResult<List<ActorGetResponse>>), 200)]
        public IActionResult GetByFirstName(string firstName)
        {
            var result = _actorService.GetByFirstName(firstName);
            if (result.Success)
            {
                return Ok(result.Data);
            }
            else
            {
                return BadRequest(result.Message);

            }
        }

        [HttpGet("id/{id}")]
        [ProducesResponseType(typeof(IDataResult<List<ActorGetResponse>>), 200)]
        public IActionResult GetByActorId(int id)
        {
            var result = _actorService.GetById(id);
            if (result.Success)
            {
                return Ok(result.Data);
            }
            else
            {
                return BadRequest(result.Message);

            }
        }

        [HttpPost]
        [ProducesResponseType(typeof(void), 200)]
        public IActionResult ActorAdd(ActorRequest request)
        {
            var result = _actorService.Add(request);
            if (result.Success)
            {
                return Ok(result.Success);
            }
            else
            {
                return BadRequest(result.Message);

            }
        }

        [HttpPut]
        [ProducesResponseType(typeof(void), 200)]
        public IActionResult ActorUpdate(ActorUpdateRequest request)
        {
            var result = _actorService.Update(request);
            if (result.Success)
            {
                return Ok(result.Success);
            }
            else
            {
                return BadRequest(result.Message);

            }
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(void), 200)]
        public IActionResult ActorDelete(int id)
        {
            var result = _actorService.Delete(id);
            if (result.Success)
            {
                return Ok(result.Success);
            }
            else
            {
                return BadRequest(result.Message);

            }
        }
    }
}
