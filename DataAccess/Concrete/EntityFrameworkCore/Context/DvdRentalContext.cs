﻿using Entities.Concrate;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using Core.Entities.Concrate;
using Entities.Concrate.Actors;

namespace DataAccess.Concrete.EntityFrameworkCore.Context
{
    public class DvdRentalContext:DbContext
    
    {
        public DvdRentalContext(DbContextOptions options):base(options)
        {
            
        }
        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    IConfigurationRoot configuration = new ConfigurationBuilder()
        //        .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
        //        .AddJsonFile("appsettings.Development.json")
        //        .Build();
        //    string connectionString = configuration.GetConnectionString("DefaultConnection");
        //    optionsBuilder.UseNpgsql(connectionString);
            
        //}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
          
            modelBuilder.Entity<Actors>()
                .HasKey(a => a.actor_id)
                .HasName("PrimaryKey_actor_id");
        }

        public DbSet<Actors> Actors { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<OperationClaim> OperationClaims { get; set; }
        public DbSet<UserOperationClaim> UserOperationClaims { get; set; }
    }
    
}
