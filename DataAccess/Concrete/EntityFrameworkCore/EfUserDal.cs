﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.DataAccess.EntityFrameworkCore;
using Core.Entities.Concrate;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFrameworkCore.Context;
using Entities.Concrate;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFrameworkCore
{
    public class EfUserDal : EfEntityRepositoryBase<User, DvdRentalContext>, IUserDal
    {
        private readonly DvdRentalContext _context;
        public List<OperationClaim> GetClaims(User user)
        {

            var result = from OperationClaim in _context.OperationClaims
                         join UserOperationClaims in _context.UserOperationClaims on OperationClaim.id
                             equals UserOperationClaims.operationclaim_id
                         where OperationClaim.id == UserOperationClaims.operationclaim_id
                         select new OperationClaim
                         {
                             id = OperationClaim.id,
                             name = OperationClaim.name
                         };
            return result.ToList();

        }

        public EfUserDal(DvdRentalContext context) : base(context)
        {
            _context = context;
        }
    }
}
