﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Core.DataAccess;
using Core.DataAccess.EntityFrameworkCore;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFrameworkCore.Context;
using Entities.Concrate;
using Entities.Concrate.Actors;

namespace DataAccess.Concrete
{
    public class EfActorDal :EfEntityRepositoryBase<Actors,DvdRentalContext>, IActorDal
    {
        public EfActorDal(DvdRentalContext context) : base(context)
        {
        }

    }
}
