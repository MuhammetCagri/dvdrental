﻿using Core.DataAccess;
using Entities.Concrate;
using System;
using System.Collections.Generic;
using System.Text;
using Entities.Concrate.Actors;

namespace DataAccess.Abstract
{//Burada Repository Pattern yapacağız proje bağlı olmadan generic türde implemente edeceğiz.
 //Bunun için core yi kuulanıyoruz.
    public interface IActorDal:IEntityRepository<Actors>
    {
    }
}
