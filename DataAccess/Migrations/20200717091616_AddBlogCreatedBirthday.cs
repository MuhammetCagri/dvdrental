﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccess.Migrations
{
    public partial class AddBlogCreatedBirthday : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Deadday",
                table: "Actors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "birthday",
                table: "Actors",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Deadday",
                table: "Actors");

            migrationBuilder.DropColumn(
                name: "birthday",
                table: "Actors");
        }
    }
}
