﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccess.Migrations
{
    public partial class RollBack : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Deadday",
                table: "Actors");

            migrationBuilder.DropColumn(
                name: "birthday",
                table: "Actors");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Deadday",
                table: "Actors",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "birthday",
                table: "Actors",
                type: "text",
                nullable: true);
        }
    }
}
