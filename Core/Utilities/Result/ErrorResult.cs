﻿namespace Core.Utilities.Result
{
    public class ErrorResult:Results
    {
        public ErrorResult(string message) : base(message, false)
        {

        }

        public ErrorResult() : base(false)
        {
        }
    }
}
