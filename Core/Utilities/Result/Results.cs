﻿namespace Core.Utilities.Result
{
    public class Results : IResult
    {
        public Results(string message,bool success):this(success)
        {
            Message = message;
        }

        public Results( bool success)
        {
            Success = success;
        }
        public string Message { get; set; }
        public bool Success { get; }
    }
}
