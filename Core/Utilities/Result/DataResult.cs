﻿namespace Core.Utilities.Result
{
    public class DataResult<T> : Results, IDataResult<T>
    {
        public DataResult(T data, string message, bool success) : base(message, success)
        {
            Data = data;
        }

        public DataResult(T data, bool success) : base(success)
        {
            Data = data;
        }
        public T Data { get; }
    }
}
