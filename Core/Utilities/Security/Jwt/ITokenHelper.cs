﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.Entities.Concrate;

namespace Core.Utilities.Security.Jwt
{
    public interface ITokenHelper
    {
        // kullanıcı bilgileri ile hangi rolde olcağını veriyoruz
        AccessToken CreateToken(User user,List<OperationClaim> operationClaims);
    }
}
