﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Core.Entities.Concrate;
using Core.Extensions;
using Core.Utilities.Security.Encyption;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace Core.Utilities.Security.Jwt
{
    public class JwtTokenHalper : ITokenHelper
    {
        public IConfiguration Configuration { get; } //
        private TokenOptions _tokenOptions;
        private DateTime _accessTokenExpirationTime;

        public JwtTokenHalper(IConfiguration configuration)
        {
            Configuration = configuration;
            _tokenOptions = Configuration.GetSection("TokenOptions").Get<TokenOptions>();
            _accessTokenExpirationTime = DateTime.Now.AddMinutes(_tokenOptions.AccessTokenExpiration);
            //Sectiondan TokenOptions Bilgilerini alıp TokenOptions nesnesine bind ediyor
        }

        public AccessToken CreateToken(User user, List<OperationClaim> operationClaims)
        {
            //token options oluştururken bize bazı bilgiler lazım oluyor ve kendi bildiğimiz özel bir anahtara ihtiyaç var.
            //Microsoft Identity Model Token kütüphanesine ihtiyaç vardırç
            var securityKey = SecurityKeyHelper.CreateSecurityKey(_tokenOptions.SecurityKey);
            var signingCredentials = SigningCredentialsHelper.SigningCredentials(securityKey);
            var jwt = CreatJwtSecurityToken(_tokenOptions, user, signingCredentials, operationClaims);
            var JwtSecurityTokenHandler = new JwtSecurityTokenHandler();
            var token = JwtSecurityTokenHandler.WriteToken(jwt);
            return new AccessToken
            {
                Token = token,
                ExpirationTime = _accessTokenExpirationTime

            };
        }

        public JwtSecurityToken CreatJwtSecurityToken(TokenOptions tokenOptions, User user,
            SigningCredentials signingCredentials, List<OperationClaim> operationClaims)
        {
            var jwt = new JwtSecurityToken(
                issuer: tokenOptions.Issuer,
                audience: tokenOptions.Audience,
                expires: _accessTokenExpirationTime,
                notBefore: DateTime.Now, //Token bilgisi şuandan önce ise geçerli değil demek.
                claims: SetClaims(user, operationClaims),
                signingCredentials: signingCredentials
            );
            return jwt;
        }

        IEnumerable<Claim> SetClaims(User user, List<OperationClaim> operationClaims)
        {
            var claims = new List<Claim>();
            claims.AddNameIdentifier(user.id.ToString());
            claims.AddName($"{user.first_name} {user.last_name}");
            claims.AddRoles(operationClaims.Select(o => o.name).ToArray());
            return claims;
        }
    }
}
