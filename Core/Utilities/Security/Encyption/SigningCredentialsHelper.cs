﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace Core.Utilities.Security.Encyption
{
    public class SigningCredentialsHelper
    {
        public static SigningCredentials SigningCredentials(SecurityKey key)
        {
            return  new SigningCredentials(key,algorithm:SecurityAlgorithms.HmacSha256Signature);
        }
    }
}
