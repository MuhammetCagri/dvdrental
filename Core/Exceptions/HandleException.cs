﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.Utilities.Result;

namespace Core.Exceptions
{
    public class HandleException:IResult
    {
        public void HandleExceptionMethod(Action action)
        {
            
            try
            {
                action.Invoke();
                Success = true;
            }
            catch (Exception exc)
            {
                Success = false;
                Message = exc.Message;
            }
        }


        public string Message { get; set; }
        public bool Success { get; set; }
    }
}
