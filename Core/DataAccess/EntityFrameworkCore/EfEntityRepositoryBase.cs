﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Core.Abstract;
using Microsoft.EntityFrameworkCore;

namespace Core.DataAccess.EntityFrameworkCore
{
    public class EfEntityRepositoryBase<TEntity, TContext> : IEntityRepository<TEntity>
    where TEntity : class, IEntity, new()
    where TContext : DbContext
    {
        private readonly TContext _context;

        public EfEntityRepositoryBase(TContext context)
        {
            _context = context;
        }

        public TEntity Get(Expression<Func<TEntity, bool>> filter)
        {

            return _context.Set<TEntity>().SingleOrDefault(filter);

        }

        public void Add(TEntity entity)
        {

            var addEntityEntry = _context.Entry(entity);
            addEntityEntry.State = EntityState.Added;
            _context.SaveChanges();

        }

        public void Delete(Expression<Func<TEntity, bool>> filter)
        {

            var deleteEntityEntry = _context.Entry(filter);
            deleteEntityEntry.State = EntityState.Deleted;
            _context.SaveChanges();

        }

        public List<TEntity> GetByName(Expression<Func<TEntity, bool>> filter)
        {

            return _context.Set<TEntity>().Where(filter).ToList();

        }

        public IList<TEntity> GetList(Expression<Func<TEntity, bool>> filter = null)
        {

            return filter == null
                ? _context.Set<TEntity>().ToList()
                : _context.Set<TEntity>().Where(filter).ToList();

        }

        public void Update(TEntity entity)
        {

            var updateEntityEntry = _context.Entry(entity);
            updateEntityEntry.State = EntityState.Modified;
            _context.SaveChanges();

        }

    }
}
