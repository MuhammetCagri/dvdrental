﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Core.Abstract;

namespace Core.DataAccess
{
    public interface IEntityRepository<TEntity> where TEntity: class,IEntity,new()
    {
        List<TEntity> GetByName(Expression<Func<TEntity, bool>> filter);
        IList<TEntity> GetList(Expression<Func<TEntity, bool>> filter = null);
        TEntity Get(Expression<Func<TEntity, bool>> filter);
        void Add(TEntity entity);
        void Update(TEntity entity);
        void Delete(Expression<Func<TEntity, bool>> filter);
    }
}
