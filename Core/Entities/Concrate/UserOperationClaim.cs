﻿using Core.Abstract;

namespace Core.Entities.Concrate
{
    public class UserOperationClaim:IEntity
    {
        public int id { get; set; }
        public int user_id { get; set; }
        public int operationclaim_id { get; set; }
    }
}
