﻿using System;
using Core.Abstract;

namespace Core.Entities.Concrate
{
    public class User:IEntity
    {
        public int id { get; set; }
        public string first_name { get; set; }
        public string email { get; set; }
        public byte[] password_salt { get; set; }
        public byte[] password_hash { get; set; }
        public string last_name { get; set; }
        public DateTime last_update{ get; set; }
        public bool active { get; set; }
    }
}
