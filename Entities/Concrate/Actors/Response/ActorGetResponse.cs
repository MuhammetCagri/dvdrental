﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Concrate.Actors.Response
{
    public class ActorGetResponse
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

    }
}
