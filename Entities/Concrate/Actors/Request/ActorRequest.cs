﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Concrate.Actors.Request
{
    public class ActorRequest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    public class ActorUpdateRequest
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
