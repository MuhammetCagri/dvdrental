﻿using System;
using System.ComponentModel.DataAnnotations;
using Core.Abstract;

namespace Entities.Concrate.Actors
{
   public class Actors:IEntity
    {
        [Key]
        public int actor_id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public DateTime last_update { get; set; }
       
    }
}
